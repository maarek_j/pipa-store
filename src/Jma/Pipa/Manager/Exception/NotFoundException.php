<?php

namespace Jma\Pipa\Manager\Exception;

/**
 * Description of NotFoundException
 *
 * @author Maarek
 */
class NotFoundException extends \RuntimeException
{
    
}
