<?php

namespace Jma\Pipa\Manager;

use Jma\Pipa\Repository\ApplicationRepository;
use Jma\Pipa\Model\Application;
use Jma\Pipa\Model\Ipa;
use Jma\Pipa\Model\Apk;
use Gaufrette\Filesystem;

/**
 * Description of ApplicationManager
 *
 * @author Maarek
 */
class ApplicationManager
{

    /**
     * @var ApplicationRepository
     */
    private $repo;

    /**
     *
     * @var Filesystem;
     */
    private $filesystem;

    function __construct(ApplicationRepository $repo, Filesystem $filesystem)
    {
        $this->repo = $repo;
        $this->filesystem = $filesystem;
    }

    public function getFiles(Application $app, $type, $buildCallback)
    {
        $keys = $this->filesystem->keys();
        $glob = \Symfony\Component\Finder\Glob::toRegex($app->getId() . '/' . $type . '/*.' . $type);

        $results = array();
        foreach ($keys as $key) {
            if (1 === preg_match($glob, $key)) {
                $results[] = $buildCallback($key);
            }
        }
        return $results;
    }

    /**
     * @param \Jma\Pipa\Model\Application $app
     * @return Ipa[]
     */
    public function getIpas(Application $app)
    {
        return $this->getFiles($app, "ipa", function($key) {
                    return new Ipa($key);
                });
    }

    /**
     * @param \Jma\Pipa\Model\Application $app
     * @return Apk[]
     */
    public function getApks(Application $app)
    {
        return $this->getFiles($app, 'apk', function($key) {
                    return new Apk($key);
                });
    }

}
