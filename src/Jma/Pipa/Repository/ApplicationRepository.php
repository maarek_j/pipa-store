<?php

namespace Jma\Pipa\Repository;

use Jma\Pipa\Model\Application;

/**
 * Description of Application
 *
 * @author Maarek
 */
class ApplicationRepository
{

    /**
     * @var Application[]
     */
    private $applications = array();

    /**
     * Add application in repo
     * @param string $id The id of application
     * @param string $name The name of application
     * @param string $bundleId iPhone Bundle identifier
     * @return \Jma\Pipa\Repository\ApplicationRepository
     */
    public function addApp($id, $name, $bundleId = null)
    {
        $this->applications[$id] = new Application($id, $name, $bundleId);
        return $this;
    }

    /**
     * Find all application
     * 
     * @return Application[]
     */
    public function findAll()
    {
        $results = array_values($this->applications);
        return $results;
    }

    /**
     * Find an application by id
     * 
     * @param string $id
     * @return Application
     */
    public function findById($id)
    {
        if (array_key_exists($id, $this->applications)) {
            return $this->applications[$id];
        }
        else {
            return null;
        }
    }

}
