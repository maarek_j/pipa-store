<?php

namespace Jma\Pipa\Model;

/**
 * Description of Application
 *
 * @author Maarek
 */
class Application
{

    protected $id;
    protected $name;
    protected $bundleId;

    public function __construct($id, $name, $bundleId = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->bundleId = $bundleId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getBundleId()
    {
        return $this->bundleId;
    }

    public function setBundleId($bundleId)
    {
        $this->bundleId = $bundleId;
    }
}
