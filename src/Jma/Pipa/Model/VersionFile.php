<?php

namespace Jma\Pipa\Model;

abstract class VersionFile
{

    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $key;

    function __construct($key)
    {
        $this->key = $key;
    }

    abstract public function getVersion();

    public function getKey()
    {
        return $this->key;
    }

}
