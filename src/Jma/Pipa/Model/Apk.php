<?php

namespace Jma\Pipa\Model;

class Apk extends VersionFile
{

    public function getVersion()
    {
        preg_match('/apk\/(.*)\.apk/', $this->getKey(), $matches);
        return $matches[1];
    }

}
