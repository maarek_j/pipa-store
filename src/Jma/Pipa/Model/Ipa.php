<?php

namespace Jma\Pipa\Model;

class Ipa extends VersionFile
{

    public function getVersion()
    {
        preg_match('/ipa\/(.*)\.ipa/', $this->getKey(), $matches);
        return $matches[1];
    }

}
