<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/* @var $app Application */

$appExist = function(Request $request) use ($app) {
    $id = $request->attributes->get("id");

    if (null == $app["app_repo"]->findById($id)) {
        $app->abort('404', "L'application n'existe pas");
    }
};

$app->get("/", function(Request $request) use ($app) {
    return $app['twig']->render('applications.twig', array(
                'applications' => $app['app_repo']->findAll()
    ));
})->bind('applications');

$app->get("/app/{id}.html", function($id) use ($app) {
            $application = $app["app_repo"]->findById($id);
            $ipas = $app['app_manager']->getIpas($application);
            $apks = $app['app_manager']->getApks($application);

            return $app['twig']->render('application.twig', array(
                        'application' => $application,
                        'ipas' => $ipas,
                        'apks' => $apks
            ));
        })
        ->before($appExist)
        ->bind('application');

$app->get("/app/{id}/ipa/{version}.plist", function($id, $version) use ($app) {
            $application = $app["app_repo"]->findById($id);
            $ipa = $app['app_manager']->getIpa($application, $version);

            return $app['twig']->render('plist.twig', array(
                        'application' => $application,
                        'ipa' => $ipa
            ));
        })
        ->before($appExist)
        ->assert('version', '[^/]+')
        ->bind('plist');

$app->get("/dl/file/{key}", function($key) use ($app) {
            return $app->stream(function() use ($key, $app) {
                        echo $app['filesystem']->read($key);
                    }, 200, array(
                        'Content-type' => 'application/octet-stream'
            ));
        })
        ->assert('key', '.+')
        ->bind("dl_file");

return $app;
