<?php

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../resources/templates',
    'twig.options' => array(
        'cache' => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
        'strict_variables' => false
    ),
));

$app['app_repo'] = $app->share(function() {
    $repo = new \Jma\Pipa\Repository\ApplicationRepository();
    return require implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', 'resources', 'config', 'applications.php'));
});

$app['app_manager'] = $app->share(function() use ($app) {
    return new \Jma\Pipa\Manager\ApplicationManager($app['app_repo'], $app['filesystem']);
});

return $app;
