<?php

$repo->addApp('monviti', 'monviti', 'fr.xmedia.monviti')
    ->addApp('artisans-aquitain', 'artisans-aquitain')
    ->addApp('circuits-culture', 'circuits-culture')
    ->addApp('cultivar-elevage', 'cultivar-elevage');

return $repo;