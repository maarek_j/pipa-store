<?php

$app['debug'] = false;

// Local
$app['locale'] = 'fr';
$app['session.default_locale'] = $app['locale'];

// Cache
$app['cache.path'] = implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'aux', 'cache'));

// Http cache
$app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

// Twig cache
$app['twig.options.cache'] = $app['cache.path'] . '/twig';


// Config app
$app['web_dir'] = realpath(implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'web')));
$app['apps_dir'] = realpath(implode(DIRECTORY_SEPARATOR, array($app['web_dir'], 'applications')));

$app['web_url'] = '/pipa-store/web';
$app['web_url_abs'] = 'http://localhost:8888/' . $app['web_url'];

$app['app_url'] = $app['web_url'] . '/applications';
$app['app_url_abs'] = $app['web_url_abs'] . '/applications';

$app['style'] = array(
    'footer' => '<a href="mailto:xmediadev@gmail.com"><u>Contacter X-Media</u></a>'
);

$app['filesystem'] = $app->share(function() use ($app) {
    $local = new \Gaufrette\Adapter\Local($app['apps_dir']);
    return new \Gaufrette\Filesystem($local);
});
